name = "Loven"
age = 31
occupation = "developer"
movie = "Huling Kembot ni Lola"
rating = 99.9
print(f"I am {name}, and I am {age} years old, I work as a {occupation}, and my rating for {movie} is {rating} %")

num1, num2, num3 = 1,2,3

print(num1*num2*num3)
print(num1<num3)
print(num3+num2)